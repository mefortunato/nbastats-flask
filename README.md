# NBA Stats

Visit <a href="http://nbastats-pye.herokuapp.com/" target="_blank">http://nbastats-pye.herokuapp.com</a>.  

This is the beginning of a project to explore NBA stats in a search to find the appropriate length of a NBA season. Initially, the pythagorean expectation was used to predict the number of wins for each team. This prediction was compared with the number of games each team actually won. After 54 games, the error in the pythagorean prediction was under 2.5 games.